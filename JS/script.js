const testWrapper = document.querySelector(".test-wrapper");
const testArea = document.querySelector("#test-area");
const originText = document.querySelector("#origin-text p").innerHTML;
const resetButton = document.querySelector("#reset");
const theTimer = document.querySelector(".timer");

// min, seconds, hundredths of seconds, thousands of seconds]
var timer = [0,0,0,0];
var interval;
var timerRunning = false;

//Add leading zero to number 9 or below (for astetics)
function leadingZero(time){
    if (time <= 9){
        time = "0" + time;
    }
    return time;
}

//Run a standard minute/second/hundredths timer:
function runTimer(){
    let currentTime = leadingZero(timer[0]) + ":" + leadingZero(timer[1]) + ":"
        + leadingZero(timer[2]);
    theTimer.innerHTML = currentTime;
    timer[3]++;

    //minutes     thousands of seconds/100 = seconds  then / 60 = minutes
    timer[0] = Math.floor((timer[3]/100)/60);
    //Seconds  thousands of seconds/100 = seconds    - value of timer (0) bc
    // it's minutes..  every time it hits 60 seconds it returns to 0
    timer[1] = Math.floor((timer[3]/100) - (timer[0] * 60));
    //   we reset to 0 when we get to hundredths of a second.
    // - also x 6000 seconds.. aka every time minutes = 100
    timer[2] = Math.floor((timer[3] - (timer[1] * 100) - (timer[0] * 6000)));



}

//Match the text entered with the provided text on the page:
function checkForWord(){

    let textEntered = testArea.value;
    let originTextMatch = originText.substring(0,textEntered.length)

    if (textEntered == originText){
        clearInterval(interval);
        testWrapper.style.borderColor = "#429890";
    }else{

        if(textEntered == originTextMatch) {

            testWrapper.style.borderColor = "#65CCf3";
        }else{
            testWrapper.style.borderColor = "#E95D0F";
        }
    }
    console.log(textEntered);
}

//Start the timer
function startTimer(){
    let textEnteredLength = testArea.value.length;
    if (textEnteredLength == 0 && !timerRunning){
        timerRunning = true;
        //JS build in function, runs every thousandth of a second(
        interval = setInterval(runTimer, 10);
    }
    console.log(textEnteredLength);
}

//Reset everything
function reset(){
    clearInterval(interval);
    interval = null;
    timer = [0,0,0,0];
    timerRunning = false;
    //.console.log("reset button has been pressed");

    testArea.value = "";
    theTimer.innerHTML = "00:00:00";
    testWrapper.style.borderColor = "grey";

}

// Event listener for keyboard input and the reset button


testArea.addEventListener("keypress", startTimer, false);
// when you let go of the key, aka "keyup"
testArea.addEventListener("keyup", checkForWord, false);
resetButton.addEventListener("click", reset, false);















